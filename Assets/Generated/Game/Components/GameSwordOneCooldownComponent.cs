//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public SwordOneCooldown swordOneCooldown { get { return (SwordOneCooldown)GetComponent(GameComponentsLookup.SwordOneCooldown); } }
    public bool hasSwordOneCooldown { get { return HasComponent(GameComponentsLookup.SwordOneCooldown); } }

    public void AddSwordOneCooldown(float newValue) {
        var index = GameComponentsLookup.SwordOneCooldown;
        var component = (SwordOneCooldown)CreateComponent(index, typeof(SwordOneCooldown));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceSwordOneCooldown(float newValue) {
        var index = GameComponentsLookup.SwordOneCooldown;
        var component = (SwordOneCooldown)CreateComponent(index, typeof(SwordOneCooldown));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveSwordOneCooldown() {
        RemoveComponent(GameComponentsLookup.SwordOneCooldown);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherSwordOneCooldown;

    public static Entitas.IMatcher<GameEntity> SwordOneCooldown {
        get {
            if (_matcherSwordOneCooldown == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.SwordOneCooldown);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherSwordOneCooldown = matcher;
            }

            return _matcherSwordOneCooldown;
        }
    }
}
