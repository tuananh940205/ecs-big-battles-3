//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameContext {

    public GameEntity playerLevelUIEntity { get { return GetGroup(GameMatcher.PlayerLevelUI).GetSingleEntity(); } }
    public PlayerLevelUIComponent playerLevelUI { get { return playerLevelUIEntity.playerLevelUI; } }
    public bool hasPlayerLevelUI { get { return playerLevelUIEntity != null; } }

    public GameEntity SetPlayerLevelUI(int newValue) {
        if (hasPlayerLevelUI) {
            throw new Entitas.EntitasException("Could not set PlayerLevelUI!\n" + this + " already has an entity with PlayerLevelUIComponent!",
                "You should check if the context already has a playerLevelUIEntity before setting it or use context.ReplacePlayerLevelUI().");
        }
        var entity = CreateEntity();
        entity.AddPlayerLevelUI(newValue);
        return entity;
    }

    public void ReplacePlayerLevelUI(int newValue) {
        var entity = playerLevelUIEntity;
        if (entity == null) {
            entity = SetPlayerLevelUI(newValue);
        } else {
            entity.ReplacePlayerLevelUI(newValue);
        }
    }

    public void RemovePlayerLevelUI() {
        playerLevelUIEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public PlayerLevelUIComponent playerLevelUI { get { return (PlayerLevelUIComponent)GetComponent(GameComponentsLookup.PlayerLevelUI); } }
    public bool hasPlayerLevelUI { get { return HasComponent(GameComponentsLookup.PlayerLevelUI); } }

    public void AddPlayerLevelUI(int newValue) {
        var index = GameComponentsLookup.PlayerLevelUI;
        var component = (PlayerLevelUIComponent)CreateComponent(index, typeof(PlayerLevelUIComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplacePlayerLevelUI(int newValue) {
        var index = GameComponentsLookup.PlayerLevelUI;
        var component = (PlayerLevelUIComponent)CreateComponent(index, typeof(PlayerLevelUIComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemovePlayerLevelUI() {
        RemoveComponent(GameComponentsLookup.PlayerLevelUI);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherPlayerLevelUI;

    public static Entitas.IMatcher<GameEntity> PlayerLevelUI {
        get {
            if (_matcherPlayerLevelUI == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.PlayerLevelUI);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherPlayerLevelUI = matcher;
            }

            return _matcherPlayerLevelUI;
        }
    }
}
