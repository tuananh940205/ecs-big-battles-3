﻿using UnityEngine;

public class DamageText : GameView {
   [SerializeField] private TextMesh textMesh;

   public void SetText(int value) {
      textMesh.text = value.ToString();
   }
}