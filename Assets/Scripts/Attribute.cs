﻿// using System;
//
// [Serializable]
// public class CharacterAttribute {
//   protected int value;
//   private Action<int> onValueChanged;
//
//   public void RegisterListener(Action<int> callback) {
//     onValueChanged += callback;
//   }
//
//   public void RemoveListener(Action<int> callback) {
//     onValueChanged -= callback;
//   }
//
//   public int Value {
//     get => value;
//     set => onValueChanged?.Invoke(value);
//   }
// }
//
// [Serializable]
// public class Strength : CharacterAttribute {
//   public Strength(int value) {
//     this.value = value;
//   }
// }
//
// [Serializable]
// public class Dexterity : CharacterAttribute {
//   public Dexterity(int value) {
//     this.value = value;
//   }
// }
//
// [Serializable]
// public class Vit : CharacterAttribute {
//   public Vit(int value) {
//     this.value = value;
//   }
// }
//
// [Serializable]
// public class MinDamage : CharacterAttribute {
// }
//
// [Serializable]
// public class MaxDamage : CharacterAttribute {
// }
//
// [Serializable]
// public class Defense : CharacterAttribute {
// }
//
// [Serializable]
// public class CurrentHealth : CharacterAttribute {
// }
//
// [Serializable]
// public class MaxHealth : CharacterAttribute {
// }
//
// [Serializable]
// public class Level : CharacterAttribute {
// }