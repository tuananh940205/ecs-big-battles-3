﻿using UnityEngine;

public class Replenish : MonoBehaviour {
   [SerializeField] private int team;

   public int Team => team;
}