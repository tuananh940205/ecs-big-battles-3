﻿using UnityEngine;

public class Fencer : CharacterView {
   public override bool Castable(int value, Vector3 position) {
      float distance;
      switch (value) {
         case 1:
            distance = 4.5f;
            break;
         case 2:
            distance = 5f;
            break;
         case 3:
            distance = 4f;
            break;
         case 4:
            distance = 7f;
            break;
         case 5:
            distance = 10f;
            break;
         default:
            return false;
      }
      if (Vector3.Distance(GameEntityView.character.value.Position, position) < distance)
         return true;
      return false;
   }

   public override void CastOne(Transform target) {
      // GameEntityView.CastAbility(target.position, AbilityMovementState.MoveFoward, 1.5f, 0,  GameConstants.AbilitiesFolders + GameConstants.SwordFolder + GameConstants.One, 0.8f, 4);
      SpawnSkill(GameConstants.AbilitiesFolders + GameConstants.SwordFolder + GameConstants.One, GetSkillPosition(1.5f, 0), Rotation, 0.8f, 4, AbilityMovementState.MoveFoward);
   }

   public override void CastTwo(Transform targetPosition) {
      //GameEntityView.CastAbility(targetPosition, AbilityMovementState.FallDown, 3, 6,  GameConstants.AbilitiesFolders + GameConstants.SwordFolder + GameConstants.Two, 2.3f, 11);
      SpawnSkill(GameConstants.AbilitiesFolders + GameConstants.SwordFolder + GameConstants.Two, GetSkillPosition(3, 6), Rotation, 2.3f, 16, AbilityMovementState.FallDown);
   }

   public override void CastThree(Transform targetPosition) {
      // entity.CastAbility(targetPosition, AbilityMovementState.MoveFoward, 1, 0, GameConstants.AbilitiesFolders + GameConstants.SwordFolder + GameConstants.SwordThree, 1.5f, 6);
      //GameEntityView.CastAbility(targetPosition, AbilityMovementState.Stay, 0, 0,  GameConstants.AbilitiesFolders + GameConstants.SwordFolder + GameConstants.Four, 2, 0);
      InitSingle();
      Contexts.sharedInstance.game.CreateEntity().AddAbilityCooldown(0.4f, InitSingle);

      void InitSingle() {
         SpawnSkill(GameConstants.AbilitiesFolders + GameConstants.SwordFolder + GameConstants.Four, GetSkillPosition(0, 0), Rotation, 2, 0, AbilityMovementState.Stay);   
      }
   }

   public override void CastFour(Transform targetPosition) {
      InitSingle();
      Contexts.sharedInstance.game.CreateEntity().AddAbilityCooldown(0.3f, InitSingle);
      // entity.CastAbility(targetPosition, AbilityMovementState.Stay, 0, 0, GameConstants.AbilitiesFolders + GameConstants.SwordFolder + GameConstants.SwordFour, 2, 0);
      //GameEntityView.CastAbility(targetPosition, AbilityMovementState.MoveFoward, 1, 0,  GameConstants.AbilitiesFolders + GameConstants.SwordFolder + GameConstants.Three, 1.5f, 6);
      void InitSingle() {
         SpawnSkill(GameConstants.AbilitiesFolders + GameConstants.SwordFolder + GameConstants.Three, GetSkillPosition(1, 0), Rotation, 1.5f, 6, AbilityMovementState.MoveFoward);   
      }
   }

   public override void CastFive(Transform targetPosition) {
      InitSingle();
      GameEntity e = Contexts.sharedInstance.game.CreateEntity();
      e.AddAbilityCooldown(0.5f, InitSingle);
      GameEntity e2 = Contexts.sharedInstance.game.CreateEntity();
      e2.AddAbilityCooldown(0.8f, InitSingle);

      void InitSingle() {
         SpawnSkill(GameConstants.AbilitiesFolders + GameConstants.SwordFolder + GameConstants.Five, GetSkillPosition(1, 0), Rotation, 2f, 7, AbilityMovementState.MoveFoward);
      }
   }

   protected override void SetMinDamage() {
      // Debug.LogError($"Set Min Damage {GetType()}");
      minDamage = 10 + (strength - 3) * 4 + (dexterity - 3) * 2.2f;
   }

   protected override void SetMaxDamage() {
      maxDamage = 15 + (strength - 3) * 4 + (dexterity - 3) * 3.4f;
   }

   protected override void SetMaxHP() {
      MaxHealth = 100 + (level - 1) * 16 + (strength - 3) * 5 + vit * 12;
   }

   protected override void SetDefense() {
      defense = 5 + (vit - 3) * 2.8f;
   }
}