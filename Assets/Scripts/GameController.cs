﻿using Entitas;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
   private Systems _systems;
   [SerializeField] private BattleConfig _config;
   [SerializeField] private Button testButton;

   void Awake() {
      // Debug.LogError($"Awake with time: {Time.realtimeSinceStartup}");
   }

   void OnEnable() {
      // Debug.LogError($"OnEnable with time: {Time.realtimeSinceStartup}");
   }

   void Start() {
      // Debug.LogError($"Start with time: {Time.realtimeSinceStartup}");
      Contexts contexts = Contexts.sharedInstance;
      _systems = CreateSystems(contexts, _config);
      _systems.Initialize();
      testButton.onClick.AddListener(ChangeTestScene);
   }

   void Update() {
      // Debug.LogError($"Update with time: {Time.realtimeSinceStartup}");
      _systems.Execute();
      _systems.Cleanup();
   }

   private Systems CreateSystems(Contexts contexts, BattleConfig config) {
      return new Feature("Total Systems")
         .Add(new InitializeSystems(contexts, config))
         .Add(new UpdateSystems(contexts))
         .Add(new ReactiveSystems(contexts))
         .Add(new CleanupSystems(contexts));
   }

   void OnDestroy() {
      testButton.onClick.RemoveAllListeners();
   }

   private void ChangeTestScene() {
      SceneManager.LoadScene($"Test");
   }
}