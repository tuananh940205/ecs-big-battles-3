﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour, ISingleton {
   private Dictionary<Type, string> nameTypeDict;
   private Dictionary<string, Stack<IPool>> poolDict;

   void Awake() {
      ServiceLocator.Instance.RegisterSingleton(this);
      poolDict = new Dictionary<string, Stack<IPool>>();
      nameTypeDict = new Dictionary<Type, string>();
   }

   public void Spawn(string assetPath, Vector3 position, Quaternion rotation, Action<IPool> onCompleted = null) {
      // Debug.Log($"Spawn assetPath: assetPath}, pos: {position}, direction: {direction}");
      // Debug.Log($"Spawn with con1: {poolDict.ContainsKey(assetPath) && poolDict[assetPath].Count > 0}");
      if (poolDict.ContainsKey(assetPath)) {
         // Debug.Log($"PoolDict has {assetPath} with count: {poolDict.Count}");
      }
      else {
         Debug.LogError($"PoolDict not contain {assetPath}");
      }

      if (poolDict.ContainsKey(assetPath) && poolDict[assetPath].Count > 0) {
         IPool result = poolDict[assetPath].Pop();
         result.Position = position;
         result.Rotation = rotation;
         onCompleted?.Invoke(result);
         return;
      }

      GameObject go = ServiceLocator.Instance.GetPrefabFromResources(assetPath);
      if (go != null) {
         GameObject prefab = Instantiate(go, position, rotation);
         IPool instance = prefab.GetComponent<IPool>();
         onCompleted?.Invoke(instance);
         if (!nameTypeDict.ContainsValue(assetPath)) {
            Type type = instance.GetType();
            // Debug.Log($"Assign nameTypeDict key: {type}, value: {assetPath}");
            nameTypeDict[type] = assetPath;
         }
         return;
      }

      Debug.LogError($"Cannot spawn GameObject from path {assetPath}");
   }

   public void Recycle<T>(T instance) where T : IPool {
      // Debug.Log($"Recycle instance of DataType: {instance.GetType()}");
      instance.SetActive(false);
      if (nameTypeDict.ContainsKey(instance.GetType()) && poolDict.ContainsKey(nameTypeDict[instance.GetType()])) {
         poolDict[nameTypeDict[instance.GetType()]].Push(instance);
         return;
      }

      Type type = instance.GetType();
      if (nameTypeDict.ContainsKey(type)) {
         string assetPath = nameTypeDict[type];
         poolDict[assetPath] = new Stack<IPool>();
         poolDict[assetPath].Push(instance);
         return;
      }

      Debug.LogError($"nameTypeDict not contain {type}");
   }

   private void OnDestroy() {
      poolDict.Clear();
      nameTypeDict.Clear();
      ServiceLocator.Instance.UnregisterSingleton(this);
   }
}