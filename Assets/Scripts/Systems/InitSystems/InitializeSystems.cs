﻿public class InitializeSystems : Feature {
   public InitializeSystems(Contexts contexts, BattleConfig config) : base("Init Systems") {
      Add(new InitializeEnvironmentSystem(contexts, config));
   }
}