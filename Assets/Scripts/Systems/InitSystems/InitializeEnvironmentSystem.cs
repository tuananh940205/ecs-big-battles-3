﻿using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using UnityEngine;

public class InitializeEnvironmentSystem : IInitializeSystem, ITearDownSystem {
   private readonly BattleConfig _config;
   private GameContext gameContext;
   private readonly GameStateContext gameStateContext;
   private GameObject charParent;
   private GameObject healthBars;
   
   private List<string> playerClasses = new List<string> {
      GameConstants.Archer,
      GameConstants.Fencer
   };

   public InitializeEnvironmentSystem(Contexts contexts, BattleConfig config) {
      _config = config;
      gameContext = contexts.game;
      gameStateContext = contexts.gameState;
      charParent = new GameObject("Characters");
      healthBars = new GameObject("healthBars");
   }

   public void Initialize() {
      Init();
      InitUI();
   }

   private void Init() {
      GameEntity follower = gameContext.CreateEntity();
      GameEntity player = gameContext.CreateEntity();
      GameEntity healthEntity = gameContext.CreateEntity();
      player.isPlayer = true;
      Vector3 initPosition = new Vector3(33f + Random.Range(0f, 0.5f), 0.1f, -31.8f + Random.Range(0, 0.5f));
      string playerClass = GameConstants.Character + GameConstants.Fencer;
      Quaternion playerRot = Quaternion.Euler(0, 180, 0);
      ServiceLocator.Instance.Resolve<ObjectPool>().Spawn(playerClass, initPosition, playerRot,  OnCharacterInit);

      gameContext.SetPlayerHealthUI(player.character.value.CurrentHealth, player.character.value.MaxHealth);
      gameContext.SetPlayerExpUI(player.character.value.CurrentExp, player.character.value.RequireExp);

      // Stop here for test
      // return;
      InitNonPlayerCharacters(15, 1);
      InitNonPlayerCharacters(15, 2);

      void OnCharacterInit(IPool instance) {
         follower.AddFollowPlayer(instance.GameObject.transform);
         instance.Team = 1;
         instance.GameObject.Link(player);
         instance.GameObject.transform.SetParent(charParent.transform);
         if (instance is CharacterView character) {
            OnCharacterInit2(character);
         }
      }

      void OnCharacterInit2(CharacterView instance) {
         player.AddCharacter(instance);
         instance.GameEntityView = player;
         ServiceLocator.Instance.Resolve<ObjectPool>().Spawn(GameConstants.HealthBar, initPosition, Quaternion.Euler(Vector3.zero), OnCharacterInit3);
         
         void OnCharacterInit3(IPool item) {
            if (item is HealthBar bar) {
               instance.HealthBar = bar;
               healthEntity.AddHealthView(bar);
               healthEntity.AddHealthBar(instance.transform);
               bar.GameObject.transform.SetParent(healthBars.transform);
            }
         }
      }
   }

   private void InitNonPlayerCharacters(int amount, int team) {
      string colorMat = team == 1 ? "Green" : "Red";
      Material color = Resources.Load<Material>($"Materials/{colorMat}");
      for (int i = 0; i < amount; i++) {
         GameEntity npc = gameContext.CreateEntity();
         GameEntity hp = gameContext.CreateEntity();
         Vector3 initPosition = team == 1
            ? new Vector3(35f + Random.Range(-6f, 6f), 0 , -28 + Random.Range(-5f, 5f))
            : new Vector3(Random.Range(87f, 100f), 0, Random.Range(23f, 36f));
            //new Vector3(97 + Random.Range(-3f, 3f), 0, 33 + Random.Range(-3f, 3f));
         ServiceLocator.Instance.Resolve<ObjectPool>().Spawn(GameConstants.Character + playerClasses[Random.Range(0, playerClasses.Count)], initPosition, Quaternion.Euler(0, 180, 0), OnCharacterInit);

         void OnCharacterInit(IPool item) {
            if (item is CharacterView bot) {
               var meshRenderer = bot.GameObject.GetComponentsInChildren<MeshRenderer>();
               foreach (var mesh in meshRenderer)
                  mesh.material = color;
               string botName = team == 1 ? "Allies" : "Enemies";
               bot.GameObject.name += $"{botName}_{i}";
               bot.GameObject.transform.SetParent(charParent.transform);
               bot.Team = team;
               bot.GameObject.Link(npc);
               npc.AddCharacter(bot);
               bot.GameEntityView = npc;
               // Add auto
               npc.AddBotInteraction(0.1f, 0);
               ServiceLocator.Instance.Resolve<ObjectPool>().Spawn(GameConstants.HealthBar, initPosition, Quaternion.Euler(Vector3.zero), OnHealthBarInit);
            }
            
            void OnHealthBarInit(IPool item2) {
               if (item2 is HealthBar bar) {
                  bot.HealthBar = bar;
                  hp.AddHealthView(bar);
                  hp.AddHealthBar(bot.transform);
                  bar.GameObject.transform.SetParent(healthBars.transform);
               }
            }
         }
      }
   }

   private void InitUI() {
      
   }

   public void TearDown() {
      gameContext.DestroyAllEntities();
      gameStateContext.DestroyAllEntities();
   }
}