﻿public class CleanupSystems : Feature {
   public CleanupSystems(Contexts contexts) : base("Cleanup Systems") {
      Add(new EntityDestroySystem(contexts));
   }
}