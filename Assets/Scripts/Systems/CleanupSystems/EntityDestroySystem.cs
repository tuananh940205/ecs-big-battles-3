﻿using Entitas;
using Entitas.Unity;

public class EntityDestroySystem : ICleanupSystem {
   private readonly IGroup<GameEntity> destroys;
   private readonly IGroup<GameEntity> destroyTexts;
   private readonly IGroup<GameEntity> effects;

   public EntityDestroySystem(Contexts contexts) {
      destroys = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Destroyed).AnyOf(GameMatcher.Ability));
      destroyTexts = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.DamageText, GameMatcher.Destroyed));
      effects = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Effect, GameMatcher.Destroyed));
   }

   public void Cleanup() {
      foreach (GameEntity e in destroys.GetEntities()) {
         e.ability.value.GameObject.Unlink();
         ServiceLocator.Instance.Resolve<ObjectPool>().Recycle(e.ability.value);
         e.Destroy();
      }

      foreach (GameEntity e in destroyTexts.GetEntities()) {
         ServiceLocator.Instance.Resolve<ObjectPool>().Recycle(e.damageText.value);
         e.Destroy();
      }

      foreach (GameEntity e in effects.GetEntities()) {
         ServiceLocator.Instance.Resolve<ObjectPool>().Recycle(e.effect.instance);
         e.Destroy();
      }
   }
}