﻿using System.Collections.Generic;
using Entitas;

public class UpdateUILevelSystem : ReactiveSystem<GameEntity> {
   private RootCanvas rootCanvas;
   
   public UpdateUILevelSystem(Contexts contexts) : base(contexts.game) {
      rootCanvas = ServiceLocator.Instance.Resolve<RootCanvas>();
   }

   protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
      return context.CreateCollector(GameMatcher.PlayerLevelUI);
   }

   protected override bool Filter(GameEntity entity) {
      return true;
   }

   protected override void Execute(List<GameEntity> entities) {
      foreach (GameEntity e in entities) {
         rootCanvas.SetLevel(e.playerLevelUI.value);
      }
   }
} 