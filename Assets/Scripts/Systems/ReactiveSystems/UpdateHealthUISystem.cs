﻿using System.Collections.Generic;
using Entitas;

public class UpdateHealthUISystem : ReactiveSystem<GameEntity> {
   private readonly GameContext gameContext;
   private RootCanvas rootCanvas;
   
   public UpdateHealthUISystem(Contexts contexts) : base(contexts.game) {
      gameContext = contexts.game;
      rootCanvas = ServiceLocator.Instance.Resolve<RootCanvas>();
   }

   protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
      return context.CreateCollector(GameMatcher.AllOf(GameMatcher.PlayerHealthUI));
   }

   protected override bool Filter(GameEntity entity) {
      return true;
   }

   protected override void Execute(List<GameEntity> entities) {
      foreach (GameEntity e in entities) {
         rootCanvas.SetHealth(e.playerHealthUI.currentValue, e.playerHealthUI.maxValue);
      }
   }
} 