﻿public class ReactiveSystems : Feature {
   public ReactiveSystems(Contexts contexts) : base("Reactive Systems") {
      Add(new UpdateHealthUISystem(contexts));
      Add(new UpdateExpUISystem(contexts));
      Add(new UpdateUILevelSystem(contexts));
   }
}