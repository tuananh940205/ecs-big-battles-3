﻿using System.Collections.Generic;
using Entitas;

public class UpdateExpUISystem : ReactiveSystem<GameEntity> {
   private RootCanvas rootCanvas;
   
   public UpdateExpUISystem(Contexts contexts) : base(contexts.game) {
      rootCanvas = ServiceLocator.Instance.Resolve<RootCanvas>();
   }

   protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
      return context.CreateCollector(GameMatcher.PlayerExpUI);
   }

   protected override bool Filter(GameEntity entity) {
      return true;
   }

   protected override void Execute(List<GameEntity> entities) {
      foreach (GameEntity e in entities) {
         rootCanvas.SetExp(e.playerExpUI.currentValue, e.playerExpUI.maxValue);
      }
   }
} 