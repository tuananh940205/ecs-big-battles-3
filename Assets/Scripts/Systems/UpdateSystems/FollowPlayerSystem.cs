﻿using Entitas;
using UnityEngine;

public class FollowPlayerSystem : IExecuteSystem {
   private GameObject obj;
   private readonly IGroup<GameEntity> followers;

   public FollowPlayerSystem(Contexts contexts) {
      obj = GameObject.Find("Follower");
      followers = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.FollowPlayer));
   }

   public void Execute() {
      if (followers.count <= 0)
         return;
      foreach (GameEntity e in followers.GetEntities()) {
         obj.transform.position = e.followPlayer.value.position;
      }
   }
}