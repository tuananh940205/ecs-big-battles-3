﻿using Entitas;

public class CharacterHealingSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> drinkers;

   public CharacterHealingSystem(Contexts contexts) {
      drinkers =
         contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Character).NoneOf(GameMatcher.Death, GameMatcher.Portion));
   }
   
   public void Execute() {
      if (drinkers.count <= 0)
         return;
      foreach (GameEntity e in drinkers.GetEntities()) {
         if (e.character.value.CanUsePortion)
            e.character.value.UsePortion();
      }
   }
}