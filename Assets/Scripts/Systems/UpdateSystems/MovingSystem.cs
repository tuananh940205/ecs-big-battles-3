﻿using Entitas;
using UnityEngine;

public class MovingSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> movers;

   public MovingSystem(Contexts contexts) {
      movers = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.TargetNavigation, GameMatcher.Character));
   }

   public void Execute() {
      foreach (GameEntity e in movers.GetEntities()) {
         // Debug.Log($"pos: {e.character.value.Position}, des: {e.character.value.Agent.destination}, distance: {e.character.value.Agent.destination - e.character.value.Position}");
         if (e.character.value.IsNavigating)
            continue;
         if (e.character.value.IsAlive) {
            e.character.value.SetTriggerAnimation(AnimatorState.Idle);
         }
         e.character.value.SetBooleanAnimation(AnimatorState.Run, false);
         e.isTargetNavigation = false;
      }
   }
}