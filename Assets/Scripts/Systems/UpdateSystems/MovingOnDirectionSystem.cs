﻿using Entitas;
using UnityEngine;

public class MovingOnDirectionSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> abilities;
   private readonly IGroup<GameEntity> texts;

   public MovingOnDirectionSystem(Contexts contexts) {
      abilities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Duration, GameMatcher.MovementSpeed).AnyOf(GameMatcher.Ability).NoneOf(GameMatcher.Destroyed, GameMatcher.DamageText, GameMatcher.AbilityFall));
      texts = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.DamageText, GameMatcher.Duration, GameMatcher.MovementSpeed).NoneOf(GameMatcher.Destroyed));
   }

   public void Execute() {
      HandleAbilities();
      HandleTexts();
   }

   private void HandleAbilities() {
      if (abilities.count <= 0)
         return;
      foreach (GameEntity e in abilities.GetEntities()) {
         float x = e.movementSpeed.value * Mathf.Sin(e.ability.value.Rotation.eulerAngles.y * Mathf.Deg2Rad) * Time.deltaTime;
         float z = e.movementSpeed.value * Mathf.Cos(e.ability.value.Rotation.eulerAngles.y * Mathf.Deg2Rad) * Time.deltaTime;
         e.ability.value.Position = new Vector3(e.ability.value.Position.x + x, e.ability.value.Position.y, e.ability.value.Position.z + z);
         e.duration.value -= Time.deltaTime;
         if (e.duration.value > 0)
            continue;
         e.isDestroyed = true;
      }
   }

   private void HandleTexts() {
      if (texts.count <= 0)
         return;
      foreach (GameEntity e in texts.GetEntities()) {
         // Sin
         float y = e.movementSpeed.value * Mathf.Sin(e.damageText.value.Rotation.eulerAngles.x * Mathf.Deg2Rad) * Time.deltaTime;;
         // Cos
         float z = e.movementSpeed.value * Mathf.Cos(e.damageText.value.Rotation.eulerAngles.x * Mathf.Deg2Rad) * Time.deltaTime;;
         e.damageText.value.Position = new Vector3(e.damageText.value.Position.x, e.damageText.value.Position.y + y, e.damageText.value.Position.z + z);
         e.duration.value -= Time.deltaTime;
         if (e.duration.value > 0)
            continue;
         e.isDestroyed = true;
      }
   }
}