﻿using Entitas;
using UnityEngine;

public class PlayerInputSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> players;
   private Camera mainCam;
   private readonly GameContext gameContext;

   public PlayerInputSystem(Contexts contexts) {
      gameContext = contexts.game;
      players = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Player, GameMatcher.Character).NoneOf(GameMatcher.Attack, GameMatcher.Death));
      mainCam = Camera.main;
   }

   public void Execute() {
      Movement();
      CastSkillOnInput();
   }

   private void Movement() {
      if (Input.GetMouseButton(0) && players.count > 0) {
      // Debug.Log($"Clicked");
         foreach (GameEntity player in players.GetEntities()) {
            if (GetPosition(out Vector3 pos)) {
               // Debug.LogError($"Click {pos}");
               player.character.value.SetCharacterDestination(pos);
            }
         }
      }
   }

   private void CastSkillOnInput() {
      if (Input.GetKey(KeyCode.Q) && players.count > 0) {
         foreach (GameEntity e in players.GetEntities()) {
            Collider[] colliders = Physics.OverlapSphere(e.character.value.Position, 15, 1 << 9);
            if (colliders.Length <= 0) {
               e.character.value.SetCharacterDestination(e.character.value.Position + new Vector3(2, 0, 2));
               continue;
            }

            Transform target = colliders[0].transform;
            foreach (Collider collider in colliders) {
               if (Vector3.Distance(e.character.value.Position, target.position) >
                   Vector3.Distance(e.character.value.Position, collider.transform.position)) {
                  target = collider.transform;
               }
            }
            
            int skillNumber = e.GetAvailableSkill();
            
            //// Use for test only
            if (e.character.value.AttackDistance > 0 && Vector3.Distance(e.character.value.Position, target.position) <= e.character.value.AttackDistance) {
               e.CastAbilityWithTargetPosition(skillNumber, target);
               continue;
            }
            ////
            
            if (skillNumber > 0 && e.character.value.Castable(skillNumber, target.position)) {
               e.CastAbilityWithTargetPosition(skillNumber, target);
               continue;
            }
            
            e.character.value.SetCharacterDestination(target.position);
         }
         return;
      }
      
      if (Input.GetKey(KeyCode.Alpha1) && players.count > 0) {
         foreach (GameEntity player in players.GetEntities()) {
            if (GetPosition(out Vector3 pos)) {
               //player.CastAbilityWithTargetPosition(1, pos);
            }
         }
         return;
      }

      if (Input.GetKey(KeyCode.Alpha2)) {
         foreach (GameEntity player in players.GetEntities()) {
            if (GetPosition(out Vector3 pos)) {
               //player.CastAbilityWithTargetPosition(2, pos);
            }
         }
         return;
      }

      if (Input.GetKey(KeyCode.Alpha3)) {
         foreach (GameEntity player in players.GetEntities()) {
            if (GetPosition(out Vector3 pos)) {
               //player.CastAbilityWithTargetPosition(3, pos);
            }
         }
         return;
      }

      if (Input.GetKey(KeyCode.Alpha4)) {
         foreach (GameEntity player in players.GetEntities()) {
            if (GetPosition(out Vector3 pos)) {
               //player.CastAbilityWithTargetPosition(4, pos);
            }
         }
         return;
      }
   }

   private bool GetPosition(out Vector3 pos) {
      Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
      if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, 1 << 0)) {
         pos = hit.point;
         return true;
      }

      pos = default;
      return false;
   }
}