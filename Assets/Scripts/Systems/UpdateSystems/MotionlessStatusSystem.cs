﻿using Entitas;
using UnityEngine;

public class MotionlessStatusSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> motionless;

   public MotionlessStatusSystem(Contexts contexts) {
      motionless = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Character, GameMatcher.Motionless));
   }
   
   public void Execute() {
      foreach (GameEntity e in motionless.GetEntities()) {
         if (e.hasDeath) {
            e.RemoveMotionless();
            continue;
         }

         if (e.motionless.duration > 0) {
            e.motionless.duration -= Time.deltaTime;
            continue;
         }
         
         e.RemoveMotionless();
      }
   }
}