﻿using Entitas;
using UnityEngine;

public class AbilityFallSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> fallAbilities;

   public AbilityFallSystem(Contexts contexts) {
      fallAbilities = contexts.game.GetGroup(GameMatcher
            .AllOf(GameMatcher.Ability, GameMatcher.AbilityFall, GameMatcher.MovementSpeed, GameMatcher.Duration)
            .NoneOf(GameMatcher.Destroyed));
   }

   public void Execute() {
      if (fallAbilities.count <= 0)
         return;
      foreach (GameEntity e in fallAbilities.GetEntities()) {
         if (e.duration.value > 0) {
            float yPos = e.ability.value.Position.y - e.movementSpeed.value * Time.deltaTime > 0
               ? e.ability.value.Position.y - e.movementSpeed.value * Time.deltaTime
               : 0;
            e.ability.value.Position = new Vector3(e.ability.value.Position.x, yPos, e.ability.value.Position.z);
            e.duration.value -= Time.deltaTime;
            continue;
         }

         e.isDestroyed = true;
      }
   }
}