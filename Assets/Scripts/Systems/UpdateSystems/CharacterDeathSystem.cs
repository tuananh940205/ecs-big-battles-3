﻿using Entitas;
using UnityEngine;

public class CharacterDeathSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> dieChars;

   public CharacterDeathSystem(Contexts contexts) {
      dieChars = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Character, GameMatcher.Death));
   }

   public void Execute() {
      foreach (GameEntity e in dieChars.GetEntities()) {
         e.death.value -= Time.deltaTime;
         if (e.death.value > 0) {
            continue;
         }

         e.character.value.Revive();
         e.RemoveDeath();
      }
   }
}