﻿public class UpdateSystems : Feature {
   public UpdateSystems(Contexts contexts) : base("Update Systems") {
      Add(new FollowPlayerSystem(contexts));
      Add(new DurationSystem(contexts));
      Add(new AttackCooldownSystem(contexts));
      Add(new PlayerInputSystem(contexts));
      Add(new CharacterUpdateSystem(contexts));
      Add(new MovingSystem(contexts));
      Add(new MovingOnDirectionSystem(contexts));
      Add(new UpdateFrameRateSystem(contexts));
      Add(new HealthBarFollowCharacterSystem(contexts));
      Add(new CharacterDeathSystem(contexts));
      Add(new AbilityFallSystem(contexts));
      Add(new AbilityCooldownSystem(contexts));
      Add(new BotBehaviorSystem(contexts));
      Add(new RaiseHealthSystem(contexts));
      Add(new ImmortalSystem(contexts));
      Add(new MotionlessStatusSystem(contexts));
      Add(new CharacterHealingSystem(contexts));
      Add(new PortionCooldownSystem(contexts));
      Add(new AbilityCastingCooldownSystem(contexts));
   }
}