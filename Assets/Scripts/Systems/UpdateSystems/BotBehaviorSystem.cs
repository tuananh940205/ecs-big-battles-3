﻿using Entitas;
using UnityEngine;

public class BotBehaviorSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> computers;

   public BotBehaviorSystem(Contexts contexts) {
      computers = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Character, GameMatcher.BotInteraction).NoneOf(GameMatcher.Player, GameMatcher.Death, GameMatcher.Attack));
   }
   
   public void Execute() {
      if (computers.count <= 0)
         return;
      foreach (GameEntity computer in computers.GetEntities()) {
         if (computer.botInteraction.cooldown > 0) {
            computer.botInteraction.cooldown -= Time.deltaTime;
            continue;
         }

         if (!computer.character.value.IsAlive)
            continue;
         computer.botInteraction.cooldown = computer.botInteraction.duration;
         int enemyLayer = computer.character.value.Team == 1 ? 9 : 8;
         if (computer.hasHealthReplenish) {
            if (computer.character.value.HealthPercentage <= 75) {
               ScanEnemy(computer, 5, 1 << enemyLayer);
               continue;
            }
         }
         if (computer.character.value.HealthPercentage <= 25) {
            Vector3 targetNav = computer.character.value.Team == 1 
               ? new Vector3(-1, 0, -1) * 2
               : new Vector3(1, 0, 1) * 2; 
            computer.character.value.SetCharacterDestination(computer.character.value.Position + targetNav);
            continue;
         }

         
         Collider[] clds = Physics.OverlapSphere(computer.character.value.Position, 10, 1 << enemyLayer);
         // Debug.LogError($"count: {clds.Length}");
         if (clds.Length <= 0) {
            Vector3 pos = computer.character.value.Team == 1 
               ? new Vector3(1, 0, 1) * 2
               : new Vector3(-1, 0, -1) * 2; 
            computer.character.value.SetCharacterDestination(computer.character.value.Position + pos);
            continue;
         }
         int ally = computer.character.value.Team == 1 ? 8 : 9;
         Collider[] allies = Physics.OverlapSphere(computer.character.value.Position, 13, 1 << ally);
         if (allies.Length * 15 < clds.Length * 10 && !computer.hasHealthReplenish && !computer.hasMotionless) {
            Vector3 targetNav = computer.character.value.Team == 1 
               ? new Vector3(-1, 0, -1) * 2
               : new Vector3(1, 0, 1) * 2; 
            computer.character.value.SetCharacterDestination(computer.character.value.Position + targetNav);
            continue;
         }
         Transform target = clds[0].transform;
         foreach (Collider cld in clds) {
            if (Vector3.Distance(computer.character.value.transform.position, target.position) >
                Vector3.Distance(computer.character.value.transform.position, cld.transform.position)) {
               target = cld.transform;
            }
         }

         int value = computer.GetAvailableSkill();
         if (value > 0 && computer.character.value.Castable(value, target.position)) {
            computer.CastAbilityWithTargetPosition(value, target);
            continue;
         }

         // Debug.LogError($"Distance: {Vector3.Distance(computer.character.value.transform.position, target.position)}");
         // if (Vector3.Distance(computer.character.value.transform.position, target.position) > 4f) {
         //    computer.character.value.SetCharacterDestination(target.position);
         //    continue;
         // }
         
         computer.character.value.SetCharacterDestination(target.position);
         
         // Debug.LogError($"name: {computer.character.value.name}, pos: {target.name}, {target.position}");
         // computer.CastAbiltyAuto(target.position);
         // Some Logic
      }
   }
   
   private void ScanEnemy(GameEntity computer, float radius, int enemyLayer) {
      Collider[] colliders = Physics.OverlapSphere(computer.character.value.Position, radius, enemyLayer);
      if (colliders.Length <= 0) {
         return;
      }
      Transform target = colliders[0].transform;
      foreach (Collider cld in colliders) {
         if (Vector3.Distance(computer.character.value.transform.position, target.position) >
             Vector3.Distance(computer.character.value.transform.position, cld.transform.position)) {
            target = cld.transform;
         }
      }
      int value = computer.GetAvailableSkill();
      if (value > 0) {
         //tempcomputer.CastAbilityWithTargetPosition(value, target.position);
      }
   }
}