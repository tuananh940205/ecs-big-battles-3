﻿using Entitas;
using UnityEngine;

public class DurationSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> durations;

   public DurationSystem(Contexts contexts) {
      durations = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Duration).NoneOf(GameMatcher.Destroyed));
   }

   public void Execute() {
      if (durations.count <= 0)
         return;
      foreach (GameEntity e in durations.GetEntities()) {
         e.duration.value -= Time.deltaTime;

            if (e.duration.value > 0)
         continue;

         e.isDestroyed = true;
      }
   }
}