﻿using Entitas;
using UnityEngine;

public class RaiseHealthSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> healers;

   public RaiseHealthSystem(Contexts contexts) {
      healers = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.HealthReplenish, GameMatcher.Character).NoneOf(GameMatcher.Death));
   }
   
   public void Execute() {
      foreach (GameEntity e in healers.GetEntities()) {
         if (e.healthReplenish.cooldown > 0) {
            e.healthReplenish.cooldown -= Time.deltaTime;
            continue;
         }

         e.character.value.CurrentHealth += e.healthReplenish.value;
         e.character.value.IncreasePortion();
         e.healthReplenish.cooldown = e.healthReplenish.delay;
      }
   }
}