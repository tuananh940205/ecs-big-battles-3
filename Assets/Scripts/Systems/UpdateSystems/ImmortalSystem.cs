﻿using Entitas;
using UnityEngine;

public class ImmortalSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> soldiers;

   public ImmortalSystem(Contexts contexts) {
      soldiers = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Character, GameMatcher.Immortal));
   }

   public void Execute() {
      foreach (GameEntity e in soldiers.GetEntities()) {
         if (e.hasDeath) {
            Dispose(e);
            continue;
         }

         if (e.immortal.duration > 0) {
            e.immortal.duration -= Time.deltaTime;
            continue;
         }
         
         Dispose(e);
      }
   }

   private void Dispose(GameEntity entity) {
      entity.character.value.SetImmortalAttribute(false);
      entity.RemoveImmortal();
   }
}