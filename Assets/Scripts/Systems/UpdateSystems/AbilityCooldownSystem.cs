﻿using Entitas;
using UnityEngine;

public class AbilityCooldownSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> swordOnes;
   private readonly IGroup<GameEntity> swordTwos;
   private readonly IGroup<GameEntity> swordThrees;
   private readonly IGroup<GameEntity> swordFours;
   private readonly IGroup<GameEntity> fiveSkills;

   public AbilityCooldownSystem(Contexts contexts) {
      GameContext gameContext = contexts.game;
      swordOnes = gameContext.GetGroup(GameMatcher.SwordOneCooldown);
      swordTwos = gameContext.GetGroup(GameMatcher.SwordTwoCooldown);
      swordThrees = gameContext.GetGroup(GameMatcher.SwordThreeCooldown);
      swordFours = gameContext.GetGroup(GameMatcher.SwordFourCooldown);
      fiveSkills = gameContext.GetGroup(GameMatcher.SkillFive);
   }
   
   public void Execute() {
      foreach (GameEntity e in swordOnes.GetEntities()) {
         if (e.swordOneCooldown.value > 0) {
            e.swordOneCooldown.value -= Time.deltaTime;
            continue;
         }
         e.RemoveSwordOneCooldown();
      }

      foreach (GameEntity e in swordTwos.GetEntities()) {
         if (e.swordTwoCooldown.value > 0) {
            e.swordTwoCooldown.value -= Time.deltaTime;
            continue;
         }
         e.RemoveSwordTwoCooldown();
      }

      foreach (GameEntity e in swordThrees.GetEntities()) {
         if (e.swordThreeCooldown.value > 0) {
            e.swordThreeCooldown.value -= Time.deltaTime;
            continue;
         }
         e.RemoveSwordThreeCooldown();
      }

      foreach (GameEntity e in swordFours.GetEntities()) {
         if (e.swordFourCooldown.value > 0) {
            e.swordFourCooldown.value -= Time.deltaTime;
            continue;
         }
         e.RemoveSwordFourCooldown();
      }

      foreach (GameEntity e in fiveSkills.GetEntities()) {
         if (e.skillFive.delay > 0) {
            e.skillFive.delay -= Time.deltaTime;
            continue;
         }
         e.RemoveSkillFive();
      }
   }
}