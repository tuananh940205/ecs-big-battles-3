﻿using Entitas;
using UnityEngine;

public class AttackCooldownSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> attackers;

   public AttackCooldownSystem(Contexts contexts) {
      attackers = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Character, GameMatcher.Attack).NoneOf(GameMatcher.Destroyed));
   }

   public void Execute() {
      if (attackers.count <= 0)
         return;
      foreach (GameEntity e in attackers.GetEntities()) {
         if (e.hasDeath) {
            e.RemoveAttack();
            continue;
         }
         
         if (e.attack.cooldown > 0) {
            e.attack.cooldown -= Time.deltaTime;
            continue;
          }
         
         e.attack.listener?.Invoke(e.attack.target);
          // Debug.Log($"rotation {e.character.value.transform.rotation}, direction: {e.character.value.Direction}");
         e.RemoveAttack();
      }
   }
}