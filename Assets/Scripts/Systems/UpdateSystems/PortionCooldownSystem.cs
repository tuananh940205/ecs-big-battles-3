﻿using Entitas;
using UnityEngine;

public class PortionCooldownSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> portions;
   
   public PortionCooldownSystem(Contexts contexts) {
      portions = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Portion));
   }
   public void Execute() {
      foreach (GameEntity e in portions.GetEntities()) {
         if (e.hasDeath) {
            e.RemovePortion();
            continue;
         }

         if (e.portion.cooldown > 0) {
            e.portion.cooldown -= Time.deltaTime;
            continue;
         }
         
         e.RemovePortion();
      }
   }
}