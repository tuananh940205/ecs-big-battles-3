﻿using Entitas;

public class HealthBarFollowCharacterSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> healthBars;

   public HealthBarFollowCharacterSystem(Contexts contexts) {
      healthBars = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.HealthBar, GameMatcher.HealthView));
   }

   public void Execute() {
      foreach (GameEntity e in healthBars.GetEntities()) {
         e.healthView.value.transform.position = e.healthBar.value.position;
      }
   }
}