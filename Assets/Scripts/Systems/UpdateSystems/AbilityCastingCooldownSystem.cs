﻿using Entitas;
using UnityEngine;

public class AbilityCastingCooldownSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> cooldowns;

   public AbilityCastingCooldownSystem(Contexts contexts) {
      cooldowns = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.AbilityCooldown).NoneOf(GameMatcher.Destroyed));
   }

   public void Execute() {
      if (cooldowns.count <= 0)
         return;
      foreach (GameEntity e in cooldowns.GetEntities()) {
         if (e.abilityCooldown.cooldown > 0) {
            e.abilityCooldown.cooldown -= Time.deltaTime;
            continue;
         }
         
         e.abilityCooldown.listener?.Invoke();
         e.isDestroyed = true;
      }
   }
}