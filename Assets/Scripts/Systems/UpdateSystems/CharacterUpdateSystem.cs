﻿using Entitas;

public class CharacterUpdateSystem : IExecuteSystem {
   private readonly IGroup<GameEntity> characters;

   public CharacterUpdateSystem(Contexts contexts) {
      characters = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Character));
   }

   public void Execute() {
      foreach (GameEntity e in characters.GetEntities()) {
      }
   }
}