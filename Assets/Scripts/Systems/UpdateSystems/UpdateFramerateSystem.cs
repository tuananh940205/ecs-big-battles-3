﻿using Entitas;

public class UpdateFrameRateSystem : IExecuteSystem {
   private readonly FramerateText instance;

   public UpdateFrameRateSystem(Contexts contexts) {
      instance = ServiceLocator.Instance.Resolve<FramerateText>();
   }

   public void Execute() {
      instance.UpdateText();
   }
}