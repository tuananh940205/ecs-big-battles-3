﻿using UnityEngine;

public class Archer : CharacterView {
   public override bool Castable(int value, Vector3 position) {
      float distance;
      switch (value) {
         case 1:
            distance = 9.5f;
            break;
         case 2:
            distance = 10f;
            break;
         case 3:
            distance = 10f;
            break;
         case 4:
            distance = 10f;
            break;
         case 5:
            distance = 10f;
            break;
         default:
            Debug.LogError($"Castable case {value} not found");
            return false;
      }
      
      if (Vector3.Distance(GameEntityView.character.value.Position, position) < distance)
         return true;
      return false;
   }

   public override void CastOne(Transform targetPosition) {
      // GameEntityView.AddSwordOneCooldown(0.4f);
      // GameEntityView.CastAbility(targetPosition, AbilityMovementState.MoveFoward, 1.5f,  0, GameConstants.AbilitiesFolders + GameConstants.BowFolder + GameConstants.One, 2, 7);
      SpawnSkill(GameConstants.AbilitiesFolders + GameConstants.BowFolder + GameConstants.One, GetSkillPosition(1.5f, 0), Rotation, 2, 7, AbilityMovementState.MoveFoward);
   }

   public override void CastTwo(Transform targetPosition) {
      SpawnSkill(GameConstants.AbilitiesFolders + GameConstants.BowFolder + GameConstants.Two, GetSkillPosition(1.7f, 0), Quaternion.Euler(Rotation.eulerAngles.x, Rotation.eulerAngles.y - 15, Rotation.eulerAngles.z), 2, 7, AbilityMovementState.MoveFoward);
      SpawnSkill(GameConstants.AbilitiesFolders + GameConstants.BowFolder + GameConstants.Two, GetSkillPosition(1.7f, 0), Rotation, 2, 7, AbilityMovementState.MoveFoward);
      SpawnSkill(GameConstants.AbilitiesFolders + GameConstants.BowFolder + GameConstants.Two, GetSkillPosition(1.7f, 0), Quaternion.Euler(Rotation.eulerAngles.x, Rotation.eulerAngles.y + 15, Rotation.eulerAngles.z), 2, 7, AbilityMovementState.MoveFoward);
   }

   public override void CastThree(Transform targetPosition) {
      InitSingle();
      GameEntity e = Contexts.sharedInstance.game.CreateEntity();
      e.AddAbilityCooldown(0.4f, InitSingle);

      void InitSingle() {
         SpawnSkill(GameConstants.AbilitiesFolders + GameConstants.BowFolder + GameConstants.Three, GetSkillPosition(2.5f, 0), Rotation, 2f, 7, AbilityMovementState.MoveFoward);
      }
   }

   public override void CastFour(Transform targetPosition) {
      SpawnSkill(GameConstants.AbilitiesFolders + GameConstants.BowFolder + GameConstants.Four, new Vector3(targetPosition.position.x, 6, targetPosition.position.z), Rotation, 2.5f, 10, AbilityMovementState.FallDown);
   }

   public override void CastFive(Transform targetPosition) {
      InitSingle();
      Contexts.sharedInstance.game.CreateEntity().AddAbilityCooldown(0.4f, InitSingle);
      Contexts.sharedInstance.game.CreateEntity().AddAbilityCooldown(0.8f, InitSingle);

      void InitSingle() {
         SpawnSkill(GameConstants.AbilitiesFolders + GameConstants.BowFolder + GameConstants.Five, GetSkillPosition(1.4f, 0), Rotation, 2f, 8, AbilityMovementState.MoveFoward);
      }
   }
}