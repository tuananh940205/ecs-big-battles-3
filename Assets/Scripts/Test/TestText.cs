﻿using UnityEngine;

public class TestText : MonoBehaviour {
   [SerializeField] private float speed;
   [SerializeField] private TextMesh text;
   void Start() {

   }

   void Update() {
      text.transform.localPosition = new Vector3(text.transform.localPosition.x, text.transform.localPosition.y + speed * Time.deltaTime, text.transform.localPosition.z);
   }
}