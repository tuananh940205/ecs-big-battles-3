﻿using System;
using UnityEngine;

public class TestController : MonoBehaviour {
   private void Start() {
      TestFather obj = GetComponent<TestFather>();
      Type type = obj.GetType();
      Debug.LogError($"obj {obj.GetType()} {type}");
      obj.Say();
   }
}