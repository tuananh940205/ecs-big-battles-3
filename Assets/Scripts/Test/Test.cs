﻿using UnityEngine;
using UnityEngine.AI;

public class Test : MonoBehaviour {
   private Camera cam;
   [SerializeField] private NavMeshAgent agent;
   [SerializeField] private Animator animator;
   [SerializeField] private bool isMoving;
   [SerializeField] private float attackDuration;

   [SerializeField] private float x;
   [SerializeField] private float y;
   [SerializeField] private float z;
   [SerializeField] private float w;

   [SerializeField] private Vector3 Angles;
   [SerializeField] private bool hasChange = false;

   private void Start() {
      cam = Camera.main;
   }

   private bool CanAttack => attackDuration <= 0;

   private void Update() {
      // transform.rotation = Quaternion.Euler(x, y, z);
      // x = transform.rotation.x;
      // y = transform.rotation.y;
      // z = transform.rotation.z;
      // w = transform.rotation.w;
      Angles = transform.rotation.eulerAngles;
      if (hasChange)
         transform.localPosition = new Vector3(x, y, z);

      HandleMoving();
      if (!agent.hasPath && isMoving) {
         animator.Play(AnimatorState.Idle.ToString());
      }
      HandleAttack();
      DecreaseCooldown();
   }

   private void HandleAttack() {
      if (Input.GetKey(KeyCode.Alpha1)) {
         if (!CanAttack)
            return;
         attackDuration = 1f;
         agent.ResetPath();
         isMoving = false;
         AnimatorState attackType = (AnimatorState) (Random.Range(0, 2));
         Debug.Log($"attackType: {attackType}");
         animator.SetTrigger(attackType.ToString());
         //int random = Random.Range(0, 2);
         //string state = random == 0 ? "Attack1" : "Attack2";
         //animator.Play(state);
         Ray ray = cam.ScreenPointToRay(Input.mousePosition);
         RaycastHit hit;
         if (Physics.Raycast(ray, out hit, 100)) {
            Vector3 target = new Vector3(hit.point.x, transform.position.y , hit.point.z);
            // transform.rotation = Quaternion.Euler(0, );
            Vector2 distance = new Vector2(hit.point.x - transform.position.x, hit.point.z - transform.position.z);
            Debug.Log($"distance: {distance}, angle: {Mathf.Rad2Deg}");
            float directionAngle = Mathf.Atan2(distance.x, distance.y) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(directionAngle, Vector3.up);
         }
      }
   }

   private void DecreaseCooldown() {
      if (attackDuration > 0)
         attackDuration -= Time.deltaTime;
   }

   private void HandleMoving() {
      if (Input.GetMouseButton(0)) {
         if (!CanAttack)
            return;
         Ray ray = cam.ScreenPointToRay(Input.mousePosition);
         RaycastHit hit;
         if (Physics.Raycast(ray, out hit)) {
            agent.SetDestination(hit.point);
            isMoving = true;
            animator.Play(AnimatorState.Run.ToString());
            // Debug.Log($"anhtran pos: {transform.position}, target: {hit.point}");
         }
      }
   }
}