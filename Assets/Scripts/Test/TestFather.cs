﻿using UnityEngine;

public class TestFather : MonoBehaviour {
   public virtual void Say() {
      Debug.LogError($"TestFather Say {GetType()}");
   }
}
