﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestChild : TestFather {
   public override void Say() {
      Debug.LogError($"TestChild Say {GetType()}");
   }
}
