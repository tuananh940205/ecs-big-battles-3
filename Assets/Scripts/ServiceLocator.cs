﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class ServiceLocator {
   private static ServiceLocator instance;
   private Dictionary<Type, ISingleton> singletons;
   private Dictionary<string, GameObject> prefabsSingletons;
   private Dictionary<string, GameObject> prefabDict;
   private Dictionary<Type, string> nameTypeDict;

   public static ServiceLocator Instance => instance ?? (instance = new ServiceLocator());

   private ServiceLocator() {
      singletons = new Dictionary<Type, ISingleton>();
      prefabsSingletons=new Dictionary<string, GameObject>();
      prefabDict = new Dictionary<string, GameObject>();
      nameTypeDict = new Dictionary<Type, string>();
   }

   public void RegisterSingleton(ISingleton obj) {
      // Debug.Log($"Register Singleton {obj.GetType()}");
      if (!singletons.ContainsKey(obj.GetType())) {
         singletons[obj.GetType()] = obj;
         return;
      }

      Debug.LogError($"Already registered {obj.GetType()}");
      // singletons[obj.GetType()] = obj;
   }

   public void UnregisterSingleton(ISingleton obj) {
      if (singletons.ContainsKey(obj.GetType())) {
         singletons.Remove(obj.GetType());
      }
   }

   public T Resolve<T>() where T : Object, ISingleton {
      if (!singletons.TryGetValue(typeof(T), out ISingleton some)) {
         Debug.LogError($"Type {typeof(T)} not registered");
         return default(T);
      }

      if (some is T value)
         return value;

      return default;
      
      // Need update
      if (singletons.ContainsKey(typeof(T)) && singletons[typeof(T)] is T instanceResult) {
         return instanceResult;
      }

      Debug.LogError($"Not existed to resolved Type: {typeof(T)}");

      if (typeof(T).IsSubclassOf(typeof(MonoBehaviour))) {
         // Handle if exist
         var instance = Object.FindObjectOfType<T>();
         if (instance != null) {
            return instance;
         }

         GameObject go = new GameObject($"Singleton - {typeof(T)}");
         var r = go.AddComponent(typeof(T));
         if (r is T instance2) {
            return instance2;
         }
         Debug.LogError($"Resolved component {typeof(T)} inherit MonoBehaviour fail");
         Object.Destroy(go);
         return null;
      }

      T result =  Activator.CreateInstance<T>();
      singletons[typeof(T)] = result;
      return result;
   }

   public GameObject GetPrefabs(string name) {
      if (prefabsSingletons.ContainsKey(name))
         return prefabsSingletons[name];

      GameObject go = Resources.Load<GameObject>($"prefabs/{name}");
      if (go != null) {
         prefabsSingletons.Add(name, go);
         return go;
      }

      Debug.LogError($"Resources not contain {name}");
      return null;
   }

   public GameObject GetPrefabFromResources(string assetPath) {
      if (prefabDict.ContainsKey(assetPath)) {
         return prefabDict[assetPath];
      }

      //GameObject go = Resources.Load<GameObject>($"Prefabs/Character");
      GameObject go = Resources.Load<GameObject>($"PREFABS/{assetPath}");
      // go = Addressables.Load<>
      if (go != null) {
         prefabDict[assetPath] = go;
         // string a = result.GetComponent<IPool>().GetType() != null
         //   ? result.GetComponent<IPool>().GetType().ToString()
         //   : "type null";
         // string b = assetPath != null ? assetPath : "AssetPath null";
         // Debug.Log($"{a} {b}");
         // Type type = result.GetComponent<IPool>().GetType();
         Type type = go.GetComponent<IPool>().GetType();
         nameTypeDict[type] = assetPath;
         return go;
      }

      Debug.LogError($"Not containing assetPath: {assetPath}");
      return null;
   }

   public string GetPoolType(IPool instance) {
      // Type ta = instance.GetType();
      // string result = nameTypeDict[ta];
      // return result;
      if (nameTypeDict.ContainsKey(instance.GetType()))
         return nameTypeDict[instance.GetType()];
      Debug.LogError($"Dictionary string-Type not contain {instance.GetType()}");
      return string.Empty;
   }
}