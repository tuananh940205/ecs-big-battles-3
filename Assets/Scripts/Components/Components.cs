﻿using System;
using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game]
public class FollowPlayerComponent : IComponent {
   public Transform value;
}

[Game]
public class DurationComponent : IComponent {
   public float value;
}

[Game]
public class MovementSpeedComponent : IComponent {
   public float value;
}

[Game]
public class DestroyedComponent : IComponent { }

[Game]
public class CharacterComponent : IComponent {
   public CharacterView value;
}

[Game]
public class AttackComponent : IComponent {
   public float cooldown;
   public Transform target;
   public Action<Transform> listener;
}

[Game]
public class PlayerComponent : IComponent { }

[Game]
public class TargetNavigationComponent : IComponent { }

[Game]
public class AbilityComponent : IComponent {
   public AbilityProjectile value;
}

[Game]
public class HealthBarComponent : IComponent {
   public Transform value;
}

[Game]
public class HealthViewComponent : IComponent {
   public HealthBar value;
}

[Game]
public class DeathComponent : IComponent {
   public float value;
}

[Game]
public class DamageTextComponent : IComponent {
   public DamageText value;
}

[Game]
public class AbilityFallComponent : IComponent { }

[Game]
public class SwordOneCooldown : IComponent {
   public float value;
}

[Game]
public class SwordTwoCooldown : IComponent {
   public float value;
}

[Game]
public class SwordThreeCooldown : IComponent {
   public float value;
}

[Game]
public class SwordFourCooldown : IComponent {
   public float value;
}

[Game]
public class HealthReplenishComponent : IComponent {
   public int value;
   public float delay;
   public float cooldown;
}

[Game]
public class BotInteractionComponent : IComponent {
   public float duration;
   public float cooldown;
}

[Game]
public class ImmortalComponent : IComponent {
   public int defense;
   public float duration;
}

[Game]
public class MotionlessComponent : IComponent {
   public float duration;
}

[Game]
public class PortionComponent : IComponent {
   public float cooldown;
}

[Game]
public class AbilityCooldownComponent : IComponent {
   public float cooldown;
   public Action listener;
}

[Game]
public class SkillFiveComponent : IComponent {
   public float delay;
}

[Game]
public class EffectComponent : IComponent {
   public BaseEffect instance;
}

[Game, Unique]
public class PlayerHealthUIComponent : IComponent {
   public int currentValue;
   public int maxValue;
}

[Game, Unique]
public class PlayerExpUIComponent : IComponent {
   public int currentValue;
   public int maxValue;
}

[Game, Unique]
public class PlayerLevelUIComponent : IComponent {
   public int value;
}