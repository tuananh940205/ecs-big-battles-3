﻿using UnityEngine;

public class CharacterData {
   public Health Health;
   public Damage Damage;
   public Defense Defense;
}

public class Health {
   
}

public class Damage {
   private int minValue;
   private int MaxValue;

   private int Value => Random.Range(minValue, MaxValue + 1);
}

public class Defense {
   public int value;
}