﻿using UnityEngine;

public class HealthBar : GameView {
   [SerializeField] private Transform greenAnchor;

   public void UpdateHealthBar(float current, float max) {
      greenAnchor.localScale = new Vector3(current / max, greenAnchor.localScale.y, greenAnchor.localScale.z);
   }
}