﻿public static class GameConstants {
   public const string AttackSpeed = "AttackSpeed";
   public const string Character = "Characters/";
   public const string Archer = "Archer";
   public const string Fencer = "Fencer";
   public const string AbilitiesFolders = "Abilities/";
   public const string BowFolder = "Bow/";
   public const string TextFolders = "DamageText/";
   public const string SwordFolder = "Sword/";
   public const string EffectFolders = "Effects/";

   public const string UpLevel = "UpLevel";

   public const string One = "One";
   public const string Two = "Two";
   public const string Three = "Three";
   public const string Four = "Four";
   public const string Five = "Five";

   public const string Second = "_2";
   public const string Third = "_3";
   
   public const string HealthBar = "HealthBar";
   public const string Yellow = "Yellow";
   public const string Red = "Red";
   public const string Normal = "Normal";
   public const string Critical = "Critical";
}