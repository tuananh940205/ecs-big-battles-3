﻿using UnityEngine;

public interface IPool {
   void SetActive(bool status);
   int Team { get; set; }
   Vector3 Position { get; set; }
   Quaternion Rotation { get; set; }
   GameObject GameObject { get; }
   GameEntity GameEntityView { get; set; }
}

public abstract class GameView : MonoBehaviour, IPool {
   private GameEntity gameEntityView;
   [SerializeField] private Collider collider;
   [SerializeField] protected int team;

   public Collider Collider => collider;
   public GameEntity GameEntityView {
      get => gameEntityView;
      set => gameEntityView = value;
   }

   public Quaternion Rotation {
      get => transform.rotation;
      set => transform.rotation = value;
   }

   public GameObject GameObject => gameObject;

   public Vector3 Position {
      get => transform.position;
      set => transform.position = value;
   }

   public void SetActive(bool status) {
      if (gameObject.activeSelf != status)
         gameObject.SetActive(status);
   }

   public virtual int Team {
      get => team;
      set => team = value;
   }
}