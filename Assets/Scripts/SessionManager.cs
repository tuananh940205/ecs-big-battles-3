﻿using System;
using UnityEngine;
using System.Collections;

public class SessionManager : MonoBehaviour, ISingleton {
   void Awake() {
      // Debug.LogError($"Awake with time: {Time.realtimeSinceStartup}");
      ServiceLocator.Instance.RegisterSingleton(this);
      DontDestroyOnLoad(gameObject);
   }

   public Coroutine StartCoroutine(IEnumerator method) {
      return base.StartCoroutine(method);
   }

   public void StopCoroutine(Coroutine coroutine) {
      base.StopCoroutine(coroutine);
   }

   void OnDestroy() {
      ServiceLocator.Instance.UnregisterSingleton(this);
   }
}