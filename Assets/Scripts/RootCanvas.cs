﻿using UnityEngine;
using UnityEngine.UI;

public class RootCanvas : MonoBehaviour, ISingleton {
   [SerializeField] private Slider playerHealth;
   [SerializeField] private Text healthText;
   [SerializeField] private Slider playerExp;
   [SerializeField] private Text expText;
   [SerializeField] private Text lvlText;
   
   private void Awake() {
      ServiceLocator.Instance.RegisterSingleton(this);
   }

   private void OnDestroy() {
      ServiceLocator.Instance.UnregisterSingleton(this);
   }

   public void SetHealth(int currentValue, int maxValue) {
      healthText.text = $"{currentValue} / {maxValue}";
      playerHealth.value = (float) currentValue / (float) maxValue;
   }

   public void SetExp(int currentValue, int maxValue) {
      expText.text = $"{currentValue} / {maxValue}";
      playerExp.value = (float) currentValue / (float) maxValue;
   }

   public void SetLevel(int value) {
      lvlText.text = $"Level {value}";
   }
}