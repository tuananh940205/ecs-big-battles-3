﻿using System;
using Entitas.Unity;
using UnityEngine;
using UnityEngine.AI;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

public abstract class CharacterView : GameView {
   [SerializeField] protected float minDamage;
   [SerializeField] protected float maxDamage;
   [SerializeField] protected float minDmgBonus;
   [SerializeField] protected float maxDmgBonus;
   [SerializeField] protected int defBonus;
   [SerializeField] protected int criticalRate;
   [SerializeField] protected float criticalRatio;
   [SerializeField] protected int level;
   [SerializeField] private int currentExp;
   [SerializeField] private int requireExp;
   [SerializeField] private int currentHealth;
   [SerializeField] private int maxHealth;
   [SerializeField] protected float defense;
   [SerializeField] protected int strength;
   [SerializeField] protected int dexterity;
   [SerializeField] protected int vit;
   [SerializeField] private int attackSpeed;
   [SerializeField] private Animator _animator;
   [SerializeField] private NavMeshAgent agent;
   [SerializeField] private HealthBar healthBar;
   [SerializeField] private float attackDistance;
   [SerializeField] private int kill = 0;
   [SerializeField] private int death = 0;
   [SerializeField] private int damageReduction;
   [SerializeField] private int portionInventory;
   [SerializeField] private float oneCooldown;
   [SerializeField] private float twoCooldown;
   [SerializeField] private float threeCooldown;
   [SerializeField] private float fourCooldown;
   [SerializeField] private float fiveCooldown;
   [SerializeField] private int gold;

   protected CharacterData data;

   public float OneCooldown => oneCooldown;
   public int TotalDefense => (int) defense + defBonus;
   public float TwoCooldown => twoCooldown;
   public float ThreeCooldown => threeCooldown;
   public float FourCooldown => fourCooldown;
   public float FiveCooldown => fiveCooldown;
   public int CriticalRate => criticalRate;
   public float CriticalRatio => criticalRatio;
   public float MinExtraDmg => minDmgBonus;
   public float MaxExtraDmg => maxDmgBonus;
   public bool IsAlive => currentHealth > 0;
   public bool IsNavigating => agent.hasPath;
   public int HealthPercentage => currentHealth * 100 / maxHealth;
   public float AttackSpeedAnimationCooldown => 100f / attackSpeed;
   public NavMeshAgent Agent => agent;

   public int CurrentExp {
      get => currentExp;
      set {
         currentExp = value;
         SetEXPUI();
      }
   }

   public abstract bool Castable(int value, Vector3 position);
   public abstract void CastOne(Transform targetPosition);
   public abstract void CastTwo(Transform targetPosition);
   public abstract void CastThree(Transform targetPosition);
   public abstract void CastFour(Transform targetPosition);
   public abstract void CastFive(Transform targetPosition);

   protected virtual void SetMinDamage() {
      minDamage = 10 + (strength - 3) * 3 + (dexterity - 3) * 2.2f;
   }

   protected virtual void SetMaxDamage() {
      maxDamage = 15 + (strength - 3) * 3 + (dexterity - 3) * 3.4f;
   }

   protected virtual void SetMaxHP() {
      MaxHealth = 100 + (level - 1) * 15 + (strength - 3) * 5 + vit * 12;
   }

   protected virtual void SetDefense() {
      defense = 5 + (vit - 3) * 2.3f;
   }
   
   public float MinDamage {
      get => minDamage;
      set => minDamage = value;
   }
   
   public float MaxDamage {
      get => maxDamage;
      set => maxDamage = value;
   }

   public float AttackDistance {
      get => attackDistance;
      set => attackDistance = value;
   }

   public float Defense {
      get => defense;
      set => defense = value;
   }

   public override int Team {
      get => team;
      set {
         team = value;
         switch (value) {
            case 1:
               gameObject.layer = 8;
               break;
            case 2:
               gameObject.layer = 9;
               break;
         }
      }
   }

   public HealthBar HealthBar {
      get => healthBar;
      set => healthBar = value;
   }

   public int Level {
      get => level;
      set {
         if (value == level + 1) {
            level = value;
            OnLevelUp(5);
            name = $"Level_{level}";
            SetLevelUI();
         }
      }
   }

   public int CurrentHealth {
      get => currentHealth;
      set {
         currentHealth = value < maxHealth ? value : maxHealth;
         UpdateHealthBar();
      }
   }

   private void UpdateHealthBar() {
      healthBar.UpdateHealthBar(currentHealth, maxHealth);
      SetHealthUI();
   }

   private void SetHealthUI() {
      if (GameEntityView.isPlayer) {
         Contexts.sharedInstance.game.ReplacePlayerHealthUI(currentHealth, maxHealth);
      }
   }

   private void SetEXPUI() {
      if (GameEntityView.isPlayer) {
         Contexts.sharedInstance.game.ReplacePlayerExpUI(currentExp, requireExp);
      }
   }

   private void SetLevelUI() {
      if (GameEntityView.isPlayer) {
         Contexts.sharedInstance.game.ReplacePlayerLevelUI(level);
      }
   }

   public int RequireExp {
      get => requireExp;
      set => requireExp = value;
   }

   public int MaxHealth {
      get => maxHealth;
      set {
         maxHealth = value;
         UpdateHealthBar();
      }
   }

   private int AttackSpeed {
      get => attackSpeed;
      set { 
         attackSpeed = value;
         _animator.SetFloat(GameConstants.AttackSpeed, value / 100f);
      }
   }

   void Start() {
      agent = GetComponent<NavMeshAgent>();
      agent.enabled = true;
      portionInventory = level * 60 / 200;
   }

   public void Killed() {
      kill++;
   }

   public void GetBounty(int level) {
      gold += Math.Max(0, level + Random.Range(level - 5, level + 6));
   }

   public void GetExtraExperience(int value, bool owner) {
      if (owner && level < 200) {
         int layer = Team == 1 ? 8 : 9;
         Collider[] clds = Physics.OverlapSphere(Position, 4, 1 << layer);
         foreach (var cld in clds) {
            CharacterView characterView = cld.GetComponent<CharacterView>();
            if (characterView != null && characterView.IsAlive) {
               characterView.GetExtraExperience(value / 3, false);
            }
         }
      }
      // if (Level >= 200)
      //    return;

      if (value < requireExp - currentExp) {
         CurrentExp += value;
         return;
      }

      int amount = value;

      while (amount > 0) {
         // Debug.LogError($"amount left: {amount}");
         if (amount >= requireExp - currentExp) {
            amount -= (requireExp - currentExp);
            Level += 1;
            continue;
         }

         CurrentExp = amount;
         break;
      }
   }

   private void OnLevelUp(int amount) {
      ServiceLocator.Instance.Resolve<ObjectPool>().Spawn(GameConstants.EffectFolders + GameConstants.UpLevel, Position, Rotation, OnCompleted);
      damageReduction = level <= 200 ? (level - 1) * 80 / 199 : 80; 
      
      void OnCompleted(IPool item) {
         if (item is BaseEffect instance) {
            GameEntity e = Contexts.sharedInstance.game.CreateEntity();
            e.AddEffect(instance);
            e.AddDuration(2);
            instance.PlayEffect();
         }
      }
      
      CurrentExp = 0;
      requireExp = requireExp * 102 / 100;
      CurrentHealth += 15;
      // MaxHealth += 15;
      int attackSpeed = 100 + (level - 1) * 300 / 199;
      AttackSpeed = attackSpeed < 300 ? attackSpeed : 300;
      criticalRate = level <= 200 ? (level - 1) * 15 / 199 : 15;
      criticalRatio = level <= 200 ? 1 + (level - 1) * 0.8f / 199 : 2;
      for (int i = 0; i < amount; i++) {
         int value = Random.Range(1, 4);
         switch (value) {
            case 1:
               strength += 1;
               break;
            case 2:
               dexterity += 1;
               break;
            case 3:
               vit += 1;
               break;
         }
         SetMinDamage();
         SetMaxDamage();
         SetMaxHP();
         SetDefense();
      }
   }

   private void OnDamageSuffered(AbilityProjectile damage) {
      damage.CollisionCapacity -= 1;
      
      int damageSuffer = (int) Random.Range(damage.MinDmg, damage.MaxDmg);
      // Debug.LogError($"init damamge: {damageSuffer}");
      bool isCrit = damage.CriticalRate >= Random.Range(1, 100);
      if (isCrit)
         damageSuffer = (int)(damageSuffer * damage.CriticalRatio);
      damageSuffer -= TotalDefense;
      damageSuffer = damageSuffer * (100 - damageReduction) / 100;
      if (damageSuffer < 1)
         damageSuffer = 1;
      if (damageSuffer > currentHealth)
         damageSuffer = currentHealth;

      string textType = Team == 1 ? GameConstants.Red : GameConstants.Yellow;
      textType += isCrit ? GameConstants.Critical : GameConstants.Normal;
      // Debug.LogError($"Text string: {textType}");
      ServiceLocator.Instance.Resolve<ObjectPool>().Spawn(GameConstants.TextFolders + textType, new Vector3(Position.x, Position.y + 3,Position.z), Quaternion.Euler(39.6f, 0, 0), OnTextInit);

      void OnTextInit(IPool item) {
         if (item is DamageText text) {
            text.SetText(damageSuffer);
            text.SetActive(true);
            GameEntity textEntity = Contexts.sharedInstance.game.CreateEntity();
            textEntity.AddDuration(3);
            textEntity.AddMovementSpeed(2);
            textEntity.AddDamageText(text);
         }
      }
      // Debug.LogError($"Damagesuffer: {damageSuffer}");
      CurrentHealth -= damageSuffer;
      // Create text base on critical boolean
      int ratio = Random.Range(0, 100);
      if (ratio < 5) {
         if (GameEntityView.hasMotionless)
            GameEntityView.motionless.duration += 1f;
         else
            GameEntityView.AddMotionless(1);
      }
      // Handle die if hp = 0
      if (IsAlive)
         return;

      damage.Caster.Killed();
      damage.Caster.GetExtraExperience(level * 10 + 15, true);
      damage.Caster.GetBounty(level);
      Die();
   }

   private void OnTriggerEnter(Collider other) {
      AbilityProjectile damage = other.GetComponent<AbilityProjectile>();
      if (damage != null && IsAlive && damage.Team != Team) {
         // Debug.Log($"Layer projectile {damage.gameObject.layer}");
         OnDamageSuffered(damage);
      }

      Replenish replenish = other.GetComponent<Replenish>();
      if (replenish != null && replenish.Team == Team && !GameEntityView.hasHealthReplenish) {
         GameEntityView.AddHealthReplenish(maxHealth / 100, 0.5f, 0);
      }
   }

   private void OnTriggerExit(Collider other) {
      Replenish replenish = other.GetComponent<Replenish>();
      if (replenish != null && GameEntityView.hasHealthReplenish) {
         GameEntityView.RemoveHealthReplenish();
      }
   }

   public void SetCharacterDestination(Vector3 target) {
      if (GameEntityView.hasMotionless)
         return;
      GameEntityView.isTargetNavigation = true;
      agent.SetDestination(target);
      SetBooleanAnimation(AnimatorState.Run, true);
   }

   public void SetTriggerAnimation(AnimatorState state) {
      _animator.SetTrigger(state.ToString());
   }

   public void SetBooleanAnimation(AnimatorState state, bool value) {
      _animator.SetBool(state.ToString(), value);
   }

   public void HeadTarget(Vector3 hitTarget) {
      StopNavigating();
      Vector2 distance = new Vector2(hitTarget.x - Position.x, hitTarget.z - Position.z);
      float direction = Mathf.Atan2(distance.x, distance.y) * Mathf.Rad2Deg;
      Rotation = Quaternion.Euler(0, direction, 0);
   }

   private void StopNavigating() {
      agent.ResetPath();
      agent.velocity = Vector3.zero;
   }

   private void Die() {
      death++;
      if (IsNavigating) {
         StopNavigating();
      }
      SetTriggerAnimation(AnimatorState.Die);
      Collider.enabled = false;
      GameEntityView.AddDeath(3);
   }

   public void Revive() {
      portionInventory = level * 60 / 200;
      CurrentHealth = maxHealth;
      transform.position = Team == 1
         ? new Vector3(35f + Random.Range(-6f, 6f), 0 , -28 + Random.Range(-5f, 5f))
         : new Vector3(Random.Range(87f, 100f), 0, Random.Range(23f, 36f));
      SetTriggerAnimation(AnimatorState.Idle);
      
      SetImmortalAttribute(true);
      GameEntityView.AddImmortal(defBonus, 8);
      Collider.enabled = true;
   }

   public bool CanUsePortion => currentHealth + GetHealth() < maxHealth && portionInventory > 0;

   public void UsePortion() {
      GameEntityView.AddPortion(0.3f);
      CurrentHealth += GetHealth();
      portionInventory--;
   }
   
   private int GetHealth() {
      if (level >= 90)
         return 900;
      if (level >= 40)
         return 600;
      if (level >= 30)
         return 350;
      if (level >= 10)
         return 50;
      return 10;
   }

   public void IncreasePortion() {
      portionInventory += 3;
   }
   
   public Vector3 GetSkillPosition(float xDistance, float yDistance) {
      float x = xDistance * Mathf.Sin((Rotation.eulerAngles.y) * Mathf.Deg2Rad);
      float z = xDistance * Mathf.Cos((Rotation.eulerAngles.y) * Mathf.Deg2Rad);
      return new Vector3(x, yDistance, z) + Position;
   }

   protected void SpawnSkill(string name, Vector3 pos, Quaternion rot, float duration, float speed, AbilityMovementState state) {
      ServiceLocator.Instance.Resolve<ObjectPool>().Spawn(name, pos, rot, CreateEntityLinked);
      
      void CreateEntityLinked(IPool iPool) {
         if (iPool is AbilityProjectile proj) {
            proj.Team = Team;
            proj.Caster = this;
            proj.SetFullCapacity(duration, speed, state);
         }
      }
   }

   public void SetImmortalAttribute(bool status) {
      int rate = status ? +1 : -1;
      defBonus += rate * 999;
      minDmgBonus += rate * 500;
      maxDmgBonus += rate * 500;
   }
}