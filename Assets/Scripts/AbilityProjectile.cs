﻿using Entitas.Unity;
using UnityEngine;

public class AbilityProjectile : GameView {
   // [SerializeField] private AbilityMovementState movementState;
   [SerializeField] private int collisionCapacity;
   [SerializeField] private int maxCap;
   [SerializeField] private CharacterView caster;
   [SerializeField] private bool existAfterFullTarget;

   public float MinDmg => caster.MinDamage + caster.MinExtraDmg;
   public float MaxDmg => caster.MaxDamage + caster.MaxExtraDmg;
   public int CriticalRate => caster.CriticalRate;
   public float CriticalRatio => caster.CriticalRatio;
   public int MaxCap => maxCap;

   public CharacterView Caster {
      get => caster;
      set => caster = value;
   }

   public int CollisionCapacity {
      get => collisionCapacity;
      set {
         collisionCapacity = value;
            if (value > 0) {
            return;
         }

         Collider.enabled = false;
         // gameEntity.isDestroyed = true;
         if (!existAfterFullTarget)
         GameEntityView.isDestroyed = true;
      }
   }

   private void OnDisable() {
      if (Collider.enabled)
      Collider.enabled = false;
   }

   // Set on ability with animation
   public void OnAnimationFinished() {
      GameEntityView.isDestroyed = true;
   }

   public void SetFullCapacity(float duration, float speed, AbilityMovementState state) {
      CollisionCapacity = MaxCap;
      Collider.enabled = true;
      SetActive(true);
      GameEntity ability = Contexts.sharedInstance.game.CreateEntity();
      GameEntityView = ability;
      ability.AddDuration(duration);
      if (speed > 0)
         ability.AddMovementSpeed(speed);
      ability.AddAbility(this);
            
      switch (state) {
         case AbilityMovementState.FallDown:
            ability.isAbilityFall = true;
            break;
         case AbilityMovementState.MoveFoward:
            break;
         case AbilityMovementState.Stay:
            break;
      }
            
      GameObject.Link(ability);
   }
}