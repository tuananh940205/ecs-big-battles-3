﻿public enum AnimatorState {
   Attack1 = 0,
   Attack2,
   Die,
   Idle,
   Run,
}

public enum AbilityMovementState {
   MoveFoward = 0,
   FallDown,
   Stay
}