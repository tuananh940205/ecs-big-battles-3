﻿using UnityEngine;

public class BaseEffect : GameView {
   [SerializeField] private ParticleSystem particleSystem;
   
   public void PlayEffect() {
      particleSystem.Stop();
      particleSystem.Play();
   }
}