﻿using UnityEngine;
using UnityEngine.UI;

public class FramerateText : MonoBehaviour, ISingleton {
   [SerializeField] private Text framerateText;
   [SerializeField] private float count;
   [SerializeField] private float coolDown;

   private void Awake() {
      ServiceLocator.Instance.RegisterSingleton(this);
      coolDown = count;
   }

   private void OnDestroy() {
      ServiceLocator.Instance.UnregisterSingleton(this);
   }

   public void UpdateText() {
      coolDown -= Time.deltaTime;
      if (coolDown <= 0) {
         framerateText.text = $"FPS: {(int) (1f / Time.deltaTime)}";
         coolDown = count;
      }
   }
}