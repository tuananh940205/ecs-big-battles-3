﻿using System.Collections.Generic;
using Entitas.Unity;
using UnityEngine;

public partial class GameEntity {
   List<AnimatorState> attackStates = new List<AnimatorState> {
      AnimatorState.Attack1,
      AnimatorState.Attack2
   };
   
   public int GetAvailableSkill() {
      if (!hasSkillFive && character.value.Level >= 80)
         return 5;
      if (!hasSwordFourCooldown && character.value.Level >= 50)
         return 4;
      if (!hasSwordThreeCooldown && character.value.Level >= 30)
         return 3;
      if (!hasSwordTwoCooldown && character.value.Level >= 10)
         return 2;
      if (!hasSwordOneCooldown)
         return 1;
      return 0;
   }
   
   public void CastAbilityWithTargetPosition(int value, Transform target) {
      character.value.HeadTarget(target.position);
      switch (value) {
         case 1:
            AddSwordOneCooldown(character.value.OneCooldown);
            AddAttack(character.value.AttackSpeedAnimationCooldown, target, character.value.CastOne);
            break;
         case 2:
            AddSwordTwoCooldown(character.value.TwoCooldown);
            AddAttack(character.value.AttackSpeedAnimationCooldown, target, character.value.CastTwo);
            break;
         case 3:
            AddSwordThreeCooldown(character.value.ThreeCooldown);
            AddAttack(character.value.AttackSpeedAnimationCooldown, target, character.value.CastThree);
            break;
         case 4:
            AddSwordFourCooldown(character.value.FourCooldown);
            AddAttack(character.value.AttackSpeedAnimationCooldown, target, character.value.CastFour);
            break;
         case 5:
            AddSkillFive(character.value.FiveCooldown);
            AddAttack(character.value.AttackSpeedAnimationCooldown, target, character.value.CastFive);
            break;
         case 6:
            break;
         case 7:
            break;
         case 8:
            break;
         case 9:
            break;
         default:
            Debug.LogError($"Cannot cast skill value index: {value}");
            return;
      }
      AnimatorState state = attackStates[Random.Range(0, attackStates.Count)];
      character.value.SetTriggerAnimation(state);
   }
   
   public void CastAbility(Vector3 hitTarget, AbilityMovementState state, float xDistance, float yDistance, string abilityPath, float duration, float speed = 0) {
      // character.value.HeadTarget(hitTarget);
      AnimatorState value = (AnimatorState)(Random.Range(0, 2));
      character.value.SetTriggerAnimation(value);
      float x = xDistance * Mathf.Sin((character.value.Rotation.eulerAngles.y) * Mathf.Deg2Rad);
      float z = xDistance * Mathf.Cos((character.value.Rotation.eulerAngles.y) * Mathf.Deg2Rad);
      Vector3 position = character.value.GetSkillPosition(xDistance, yDistance);
      // AddAttack(character.value.AttackSpeedAnimationCooldown, abilityPath, Callback1);

      void Callback1(string assetName) {
         ServiceLocator.Instance.Resolve<ObjectPool>().Spawn(assetName, position, character.value.Rotation, InitAbility);
      }

      void InitAbility(IPool item) {
         if (item is AbilityProjectile abi) {
            abi.Team = character.value.Team;
            abi.CollisionCapacity = abi.MaxCap;
            abi.Caster = character.value;
            abi.Collider.enabled = true;
            abi.SetActive(true);
            GameEntity ability = Contexts.sharedInstance.game.CreateEntity();
            abi.GameEntityView = ability;
            ability.AddDuration(duration);
            if (speed > 0)
               ability.AddMovementSpeed(speed);
            ability.AddAbility(abi);
            
            switch (state) {
               case AbilityMovementState.FallDown:
                  ability.isAbilityFall = true;
                  break;
               case AbilityMovementState.MoveFoward:
                  break;
               case AbilityMovementState.Stay:
                  break;
            }
            
            abi.GameObject.Link(ability);
         }
      }
   }
}